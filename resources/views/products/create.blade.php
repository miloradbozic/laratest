@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Products</div>

                <div class="panel-body">

                    <span style='color:red'>{{$error or "" }}</span>
                    
                    <h1>Create Product</h1>

                    @if ( isset($product) )
                        {!! Form::model($product, ['route' => ['products.update', $product->id], 'method' => 'patch']) !!}
                    @else
                        {!! Form::open(['route' => 'products.store']) !!}
                    @endif

                        <div class="form-group">
                            {!! Form::label('name', 'Name:', array('class' => 'control-label')) !!}
                            {!! Form::text('name', null, array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Description:', array('class' => 'control-label')) !!}
                            {!! Form::textarea('description', null, array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('tags', 'Tags (comma separated):', array('class' => 'control-label')) !!}
                            {!! Form::text('tags', null, array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Save', array('class' => 'btn btn-success')) !!}
                            <a class="btn btn-default" style="margin-left:10px;"href="{{url('products')}}">Go back</a>
                        </div>
                    {!! Form::close() !!}

                    @if ($errors->any())
                        <ul>
                            {!! implode('', $errors->all('<li class="error" style="color:red">:message</li>')) !!}
                        </ul>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
@endsection

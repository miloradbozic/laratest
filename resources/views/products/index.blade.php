@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Products</div>

				<div class="panel-body">
					<a class="btn btn-primary pull-right" href="{{url('products/create')}}" style="margin-bottom:10px;">Create product</a>
					<table class="table">
						<thead>
							<tr>
								<th>Name</th>
								<th>Description</th>
								<th>Tags</th>
								<th>User</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($products as $product)
						    <tr>
						    	<td>{{$product->name}}</td>
						    	<td>{{$product->description}}</td>
						    	<td>
									{{ implode(", ", $product->tags->lists('name')) }}
						    	</td>
						    	<td>{{$product->user->name}}</td>
						    	<td>
						    		<a href="{{url('products/' . $product->id . '/edit')}}" class="btn btn-default pull-left">Edit</a>
						    		<div class="pull-left" style="margin-left:10px;">
						    		{!! Form::open(array('method' => 'DELETE', 'route' => array('products.destroy', $product->id))) !!}
									{!! Form::submit('Delete', array('class' =>'btn btn-danger')) !!}
									{!! Form::close() !!}
									</div>

						    	</td>
						    </tr>
						    @endforeach
						</tbody>
					</table>


				</div>
			</div>
		</div>
	</div>
</div>
@endsection

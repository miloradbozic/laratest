<?php namespace Laratest\Models;

use Validator;
use Lio\Core\Exceptions\NoValidationRulesFoundException;
use Lio\Core\Exceptions\NoValidatorInstantiatedException;

abstract class AbstractModel extends \Illuminate\Database\Eloquent\Model
{
	protected $validationRules = [];
    protected $validator;
    protected $validationMessages;

    public function getValidationRules()
    {
    	return $this->validationRules;
    }
    
    public function isValid()
    {
        if ( ! isset($this->validationRules)) {
            throw new NoValidationRulesFoundException('no validation rule array defined in class ' . get_called_class());
        }

        if($this->validationMessages) {
        	$this->validator = Validator::make($this->getAttributes(), $this->validationRules, $this->validationMessages);
        } else {
        	$this->validator = Validator::make($this->getAttributes(), $this->validationRules);
        }

        return $this->validator->passes();
    }

    public function getErrors()
    {
        if ( ! $this->validator) {
            throw new NoValidatorInstantiatedException;
        }

        return $this->validator->errors();
    }

/*
    public function save(array $options = array())
    {
        if ( ! $this->isValid()) {
            return false;
        }
        return parent::save($options);
    }
*/
}
<?php namespace Laratest\Models;

use Laratest\Models\Model;
use Laratest\Models\Tag;

class Product extends AbstractModel {

	protected $guarded = array('id');
	protected $fillable = array('name','description');

    protected $validationRules = [
        'name' => 'required|min:3',
		'description' => 'required|min:10'
    ];

	public function tags()
	{
		return $this->hasMany('Laratest\Models\Tag');
	}

	public function user()
    {
        return $this->belongsTo('Laratest\Models\User');
    }

    public function associateUser(User $user)
    {
    	$this->user()->associate($user);
    }

    public function saveTag(Tag $tag)
    {
    	return $this->tags()->save($tag);
    }

    public function saveTags(Collection $tags)
    {
    	return $this->tags()->saveManu($tags);
    }



}

<?php namespace Laratest\Models;

use Laratest\Models\Model;

class Tag extends AbstractModel {

	protected $guarded = array('id');
	protected $fillable = array('name','product_id');

    protected $validationRules = [
        'name' => 'unique:tags',
        //'product_id' => 'required' //we don't include this because we will be adding tags through product, so no need to set it in advance
    ];

    protected $validationMessages = [
        'name.unique' => 'Tag is already taken.',
    ];

/*
    public static $rules = [
        
    ];*/

	/** Has to be declared explicity, we can't call $tag->product($product) from controller **/
	public function setProduct(Product $product)
	{
		$this->product_id = $product->id;	//If we do it like this: $this->product = $product; , than on save we will get error that product doesnt exit as a column
	}

	public function product()
    {
        return $this->belongsTo('Laratest\Models\Product');
    }

}

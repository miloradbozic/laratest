<?php namespace Laratest\Http\Controllers;

use \Redirect;
use Laratest\Http\Controllers\Controller;
use Laratest\Http\Requests\CreateUpdateProductRequest;
use Laratest\Services\Product as ProductService;
use Laratest\Repositories\Interfaces\ProductRepositoryInterface;
use \Auth;

class ProductsController extends Controller
{
    private $productService;
    private $productRepo;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(ProductService $productService, ProductRepositoryInterface $productRepo)
	{
		$this->middleware('auth');
        $this->productService = $productService;
        $this->productRepo = $productRepo;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = $this->productRepo->listOrderedByCreation('desc');
		return view('products.index', compact('products'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('products.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateUpdateProductRequest $request)
	{
        try{
            $this->productService->store($request->all(), Auth::user());
        } catch(\ValidationException $e) {
    		return redirect()->back()->withInput()->withErrors($e->getErrors());  //something like this
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withError('Unexpected error: ' . $e->getMessage());  //something like this
        }

        return redirect('products');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$product =  $this->productRepo->find($id);
		$product->tags = implode(", ", $product->tags->lists('name'));
		return view('products.create', compact('product'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id, CreateUpdateProductRequest $request
	 * @return Response
	 */
	public function update($id, CreateUpdateProductRequest $request)
	{
        try{
            $this->productService->update($id, $request->all(), Auth::user());
        } catch(\ValidationException $e) {
            return redirect()->back()->withInput()->withErrors($e->getErrors());  //something like this
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['any' => 'Unexpected error: ' . $e->getMessage() . json_encode($e->getTraceAsString())]);  //something like this
        }

		return Redirect::route('products.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$product = $this->productRepo->find($id);
		$this->productRepo->delete($product);

		return Redirect::route('products.index');
	}
}

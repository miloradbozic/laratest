<?php namespace Laratest\Helpers;

class StringHelper
{
	/**
	* Separates tags input by comma,space or newline and returns and array
	**/
	public static function inputToArray($tags)
	{
		return preg_split('/[,;\ \v]+/', $tags);
	}

	
}
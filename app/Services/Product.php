<?php namespace Laratest\Services;

use Laratest\Helpers\StringHelper;
use Laratest\Repositories\Interfaces\ProductRepositoryInterface;
use Laratest\Repositories\Interfaces\TagRepositoryInterface;
use Mail;
use Log;
use DB;
use Laratest\Models\User;

class Product  
{
    private $productRepo;
    private $tagRepo;

    public function __construct(ProductRepositoryInterface $productRepo, TagRepositoryInterface $tagRepo)
    {
        $this->productRepo = $productRepo;
        $this->tagRepo = $tagRepo;
    }

	public function store(array $data, User $user)
    {
        DB::transaction(function() use($data, $user)
        {
            $product = $this->productRepo->newInstance($data);
            $product->associateUser($user);
            $this->productRepo->save($product);
            
            $tags = StringHelper::inputToArray($data['tags']);
            foreach ($tags as $tag) {
                $tag = $this->tagRepo->newInstance(['name' => $tag]);

                if ( ! $tag->isValid()) {
                    throw new ModelValidationException(['message' => 'Tag not valid' , 'errors' => $tag->getErrors()]);   //something like this
                }

                $tag = $product->saveTag($tag);
            }

            Mail::later(5, 'emails.storeProduct', ['productName' => $product->toArray()], function($message) use($user, $product)
            {
                $message->to($user->email, $user->name)->subject('New product created');
            });

            Log::info('New product for user ' . $user->email . ' created with name: ' . $product->name);
        });
    }

    public function update($productId, array $data, User $user)
    {
        DB::transaction(function() use($productId, $data, $user)
        {
            //throw new \Exception("Stupid exception");
            $product = $this->productRepo->find($productId);
            $product->fill($data);
            $this->productRepo->save($product); 
            
            $product->tags()->delete();
            $tags = StringHelper::inputToArray($data['tags']);
            foreach ($tags as $tag) {
                $tag = $this->tagRepo->newInstance(['name' => $tag]);

                if ( ! $tag->isValid()) {
                    throw new ModelValidationException(['message' => 'Tag not valid' , 'errors' => $tag->getErrors()]);   //something like this
                }

                $tag = $product->saveTag($tag);
            }

            Mail::later(5, 'emails.updateProduct', ['productName' => $product->toArray()], function($message) use($user, $product)
            {
              $message->to($user->email, $user->name)->subject('New product created');
            });

            Log::info('Update product for user ' . $user->email . ' with name: ' . $product->name);
        });
    }

}

<?php namespace Laratest\Repositories;

use Laratest\Models\Tag;
use Laratest\Repositories\Interfaces\TagRepositoryInterface;

class TagRepository extends AbstractEloquentRepository implements TagRepositoryInterface
{   
    protected $model;

    public function __construct(Tag $model)
    {
        $this->model = $model;
    }
}
<?php namespace Laratest\Repositories;

use Laratest\Models\Product;
use Laratest\Repositories\Interfaces\ProductRepositoryInterface;
use \Cache;

class CachedProductRepository extends AbstractEloquentRepository implements ProductRepositoryInterface
{
	public function __construct(Product $model)
	{
		$this->model = $model;
	}

	public function all()
	{
		return Cache::remember('products.all', 10, function()
		{
		    return $this->model->all();
		});
	}

	public function find($id)
	{
		return Cache::remember('products.' . $id, 10, function() use ($id)
		{
		    return $this->model->find($id);
		});
	}

	public function listOrderedByCreation($direction = 'asc')
	{
		return Cache::remember('products.orderedByCreation', 10, function() use ($direction)
		{
			return $this->model->orderBy('created_at', $direction)->limit(10)->get();
		});
	}

	public function save($data)
	{
		//Cache::tags('products')->flush();  //with this we could flush all similar caches if we used Memcached or Redis (doesn't work for FileSotre cache)
		Cache::forget('products.all');
		Cache::forget('products.orderedByCreation');
		Cache::forget('products.' . $data['id']);

		return parent::save($data);
	}

    public function delete($product)
    {
        //Cache::tags('products')->flush();  //with this we could flush all similar caches if we used Memcached or Redis (doesn't work for FileSotre cache)
        Cache::forget('products.all');
        Cache::forget('products.orderedByCreation');
        Cache::forget('products.' . $product['id']);

        parent::delete($product);
    }
}
<?php namespace Laratest\Repositories;

use Laratest\Models\Product;
use Laratest\Repositories\Interfaces\ProductRepositoryInterface;

class ProductRepository extends AbstractEloquentRepository implements ProductRepositoryInterface
{	
	protected $model;

	public function __construct(Product $model)
	{
		$this->model = $model;
	}

	public function listOrderedByCreation($direction = 'asc')
	{
		return $this->model->orderBy('created_at', $direction)->limit(10)->get();
	}
}
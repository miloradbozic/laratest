<?php namespace Laratest\Repositories;

use \Illuminate\Database\Eloquent\Model;
class AbstractEloquentRepository
{
  /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    public function __construct($model = null)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function allPaginated($count)
    {
        return $this->model->paginate($count);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function findOrfail($id)
    {
        $model = $this->findOrFail($id);
        return $model;
    }

    public function newInstance($attributes = array())
    {
        return $this->model->newInstance($attributes);
    }

    public function save($data)
    {
        if ($data instanceOf Model) {
            return $this->storeEloquentModel($data);
        } elseif (is_array($data)) {
            return $this->storeArray($data);
        }
    }

    public function delete($model)
    {
        return $model->delete();
    }

    protected function storeEloquentModel($model)
    {
        if ($model->getDirty()) {
            return $model->save();
        } else {
            return $model->touch();
        }
    }

    protected function storeArray($data)
    {
        $model = $this->getNew($data);
        return $this->storeEloquentModel($model);
    }

}

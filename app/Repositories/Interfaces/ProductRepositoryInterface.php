<?php namespace Laratest\Repositories\Interfaces;

use Laratest\Models\Product;

interface ProductRepositoryInterface extends AbstractRepositoryInterface
{	
	public function listOrderedByCreation($direction = 'asc');
}

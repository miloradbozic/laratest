<?php namespace Laratest\Repositories\Interfaces;

interface AbstractRepositoryInterface
{
    public function all();

    public function allPaginated($count); 

    public function find($id);

    public function findOrfail($id);

    public function newInstance($attributes = array());

    public function save($data);

    public function delete($model);
}
